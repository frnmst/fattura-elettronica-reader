#
# constants.py
#
# Copyright (c) 2018 Enio Carboni - Italy
# Copyright (C) 2019-2024 Franco Masotti (franco \D\o\T masotti {-A-T-} tutanota \D\o\T com)
#
# This file is part of fattura-elettronica-reader.
#
# fattura-elettronica-reader is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fattura-elettronica-reader is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fattura-elettronica-reader.  If not, see <http://www.gnu.org/licenses/>.
#
"""A file that contains all the global constants."""

from pathlib import Path

common_defaults = {
    'home_directory': Path.home(),
}

xml = {
    'metadata_file': {
        'default':
        'http://ivaservizi.agenziaentrate.gov.it/docs/xsd/fattura/messaggi/v1.0',
        'tags': {
            'invoice_checksum': 'Hash',
            'invoice_filename': 'NomeFile',
            'system_id': 'IdentificativoSdI',
        },
    },
    'trusted_list_file': {
        'namespaces': {
            'default': 'http://uri.etsi.org/02231/v2#',
        },
        'tags': {
            'certificate': 'X509Certificate',
        },
    },
    'invoice_file': {
        'namespaces': {
            'default':
            'http://ivaservizi.agenziaentrate.gov.it/docs/xsd/fatture/v1.2',
        },
        'tags': {
            'attachment': 'Attachment',
            'attachment_filename': 'NomeAttachment',
        },
        'xpath': {
            'attachment': './FatturaElettronicaBody/Allegati'
        },
        'proprieties': {
            # See:
            # https://www.fatturapa.gov.it/export/fatturazione/sdi/fatturapa/v1.2.1/Schema_del_file_xml_FatturaPA_versione_1.2.1.xsd
            'text_encoding': 'UTF-8',
        },
    },
}

# Download urls.
downloads = {
    'invoice_file': {
        'xslt': {
            # Pubblica Amministrazione.
            'pa':
            'https://www.fatturapa.gov.it/export/documenti/fatturapa/v1.2.1/Foglio_di_stile_fatturaPA_v1.2.1.xsl',
            'ordinaria':
            'https://www.fatturapa.gov.it/export/documenti/fatturapa/v1.2.1/Foglio_di_stile_fatturaordinaria_v1.2.1.xsl',
        },
        'xsd': {
            'default':
            'https://www.fatturapa.gov.it/export/documenti/fatturapa/v1.2.1/Schema_del_file_xml_FatturaPA_versione_1.2.1.xsd',
            'w3c_schema_for_xml_signatures':
            'https://www.w3.org/TR/2002/REC-xmldsig-core-20020212/xmldsig-core-schema.xsd',
        },
    },
    'trusted_list_file': {
        'default': 'https://eidas.agid.gov.it/TL/TSL-IT.xml',
    },
}

# File patches.
patch = {
    'invoice_file': {
        'xsd': {
            'line': {
                0: {
                    'offending':
                    2 * ' ' +
                    '<xs:import namespace="http://www.w3.org/2000/09/xmldsig#" schemaLocation="http://www.w3.org/TR/2002/REC-xmldsig-core-20020212/xmldsig-core-schema.xsd" />\n',
                    'fix':
                    2 * ' ' +
                    '<xs:import namespace="http://www.w3.org/2000/09/xmldsig#" schemaLocation="xmldsig-core-schema.xsd"/>\n'
                },
            },
        },
    },
}

# Relative paths.
paths = {
    'trusted_list_file': 'trusted_list.xml',
    'ca_certificate_pem_file': 'CA.pem',
    'invoice_file': {
        # Invoice stylesheet files.
        'xslt': {
            'pa': 'invoice_stylesheet_PA.xslt',
            'ordinaria': 'invoice_stylesheet_ordinaria.xslt',
        },
        # Invoice schema files (xsd).
        'xsd': {
            'default': 'invoice_schema.xsd',
            'w3c_schema_for_xml_signatures': 'xmldsig-core-schema.xsd',
        },
    },
    'configuration_file': 'fattura_elettronica_reader.conf',
}

# Stuff related generically to files.
file = {
    'invoice': {
        'attachment': {
            'extension_whitelist': ['PDF', 'pdf'],
            # Uses mimes.
            'filetype_whitelist': ['application/pdf'],
        },
    },
}

# SHA-512 checksum of the assets.
checksum = {
    paths['invoice_file']['xslt']['pa']:
    'a93dbd93fe8f3beac9ab1ea6ef322c0fdcc27b47e911a4a598c6c12c2abfb1d2ff41c406373d36ccb5d4613c36e21d09421983b5616b778573305f9bb6e3456b',
    paths['invoice_file']['xslt']['ordinaria']:
    '2c315cbb04126e98192c0afa585fe3b264ed4fada044504cf9ad77f2272e26106916239e86238dc250f15f5b22a33383e2e690ae28a5f5eb7a8a3b84d3f393b3',
    # Checksum of the patched schema file, not of the original one which is
    # 2a7c3f2913ee390c167e41ae5618c303b481f548f9b2a8d60dddc36804ddd3ebf7cb5003e5cc6996480c67d085b82b438aff7cc0f74d7c104225449785cb575b
    #
    # The xml schema file for FatturaPA version 1.2.1 needs to be patched. fattura_elettronica_reader
    # runs the SHA-512 checksum on the patched version of that file which corresponds to:
    paths['invoice_file']['xsd']['default']:
    'a1b02818f81ac91f35358260dd12e1bf4480e1545bb457caffa0d434200a1bd05bedd88df2d897969485a989dda78922850ebe978b92524778a37cb0afacba27',
    # TSL-IT.xml
    paths['trusted_list_file']:
    '6c3ac28d370d363dafedab42a794368608eda716339058f43dd604589dad38769cd88d54f15e384f406debb24fb6e1d1cfd7d78a2f33bbe7368e5ec7888e3348',
}

docs = {
    'assets_url':
    'https://docs.franco.net.eu.org/fattura-elettronica-reader/assets.html'
}

if __name__ == '__main__':
    pass
