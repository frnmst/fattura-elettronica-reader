Assets
======

fattura-elettronica-reader needs to download some files from the
*Agenzia delle Entrate* website. These files cannot be included in this
repository because of copyright. See
https://www.fatturapa.gov.it/it/copyright/index.html

Have a look at the
`fattura-elettronica-reader-assets-checksums <https://software.franco.net.eu.org/frnmst/fattura-elettronica-reader-assets-checksums>`_
repository that tracks their checksum.

Some of these file are checked in the pipeline:
if the checksum does not match the ones present in the source code an exception is raised.
You can use the ``--ignore-assets-checksum`` option to override this behaviour.

Historical Updates
------------------

October 2020
````````````

The original URL of the schema file dissapeared.

.. image:: assets/old_link.png
   :alt: Old link

A new URL points to the schema:

- https://www.fatturapa.gov.it/export/documenti/fatturapa/v1.2.1/Schema_del_file_xml_FatturaPA_versione_1.2.1a.xsd

This resource has been ``Last-Modified: Tue, 20 Oct 2020 19:29:21 GMT`` according to the HTTP headers.

.. image:: assets/page_information.png
   :alt: Page information

Same goes for the XML stylesheet files:

- FatturaPA: ``Last-Modified: Tue, 20 Oct 2020 18:00:14 GMT``
- Fattura Ordinaria: ``Last-Modified: Tue, 20 Oct 2020 18:08:05 GMT``

June 2019
`````````

According to the server the schema was last modified on
``Tue, 25 Jun 2019 10:16:31 GMT``, so they fixed an offending ``xsd`` typo:

- https://www.fatturapa.gov.it/export/fatturazione/sdi/fatturapa/v1.2.1/Schema_del_file_xml_FatturaPA_versione_1.2.1.xsd

Before June 2019
````````````````

Initially the schema file was reported as incorrect by lxml because of a typo.
