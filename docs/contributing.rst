Contributing
============

See `Python contributing <https://docs.franco.net.eu.org/ftutorials/en/content/programming/python/contributing.html>`_

TODO and FIXME
--------------

Important stuff
```````````````
- unit tests: Find usable XML examples or generate them.
- GUI: for example https://github.com/chriskiehl/Gooey
- i18n (both CLI and GUI): https://docs.python.org/3/library/gettext.html
